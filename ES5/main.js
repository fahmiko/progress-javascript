import variable from './modules/variable.js'
import datatypes from './modules/datatypes.js'
import conditional from './modules/conditional.js'
import looping from './modules/looping.js'
import stringmanipulation from './modules/stringmanipulation.js'
import arrays from './modules/arrays.js'
import spread from './modules/spread_syntax.js'
import _document from './modules/document.js'
import _timer from './modules/timer.js'
import _conversion from './modules/conversion.js'

import {
    rget,
    rpost,
    rlast
} from './modules/request.js'

import {
    promise,
    _async
} from './modules/promise.js'

var layout_coba1 = document.getElementById('coba1')
var layout_coba2 = document.getElementById('coba2')

layout_coba1.style.display = 'none'
layout_coba2.style.display = ''


document.getElementById('btn_coba1').addEventListener('click', function () {
    layout_coba1.style.display = ''
    layout_coba2.style.display = 'none'
})

document.getElementById('btn_coba2').addEventListener('click', function () {
    layout_coba1.style.display = 'none'
    layout_coba2.style.display = ''
})

setInterval(_timer, 10)
stringmanipulation()
arrow_default()
conditional()
datatypes()
variable()
promise()
looping()
arrays()
spread()

_conversion()
_document()
_async()


// Function
function arrow_default() {
    document.getElementById('function_default').innerHTML = "function default"
}

// Arrow Function
let arrow = () => "function arrow"

document.getElementById('function_arrow').innerHTML = arrow()

// Object
var car = {
    name: '',
    type: '',
    car_type: function () {
        return this.type + " " + this.name
    }
}

car.name = "Avanza"
car.type = "Toyota"

document.getElementById('object').innerHTML = `
    Oject = ` + JSON.stringify(car) + `
    Object method : ${car.car_type()}
`

// Get Data request
document.getElementById('rgetdata').addEventListener('click', () => {
    rget()
})

// Post Data request
document.getElementById('submit_form').addEventListener('click', () => {
    let title = document.getElementById('title').value
    let body = document.getElementById('body').value
    let post = {
        id: 1,
        title: title,
        body: body,
        userId: 1
    }

    rpost(post)
})

// Get Last input
document.getElementById('lastinsert').addEventListener('click', () => {
    rlast()
})