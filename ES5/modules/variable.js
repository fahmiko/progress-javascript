export default function variable() {
    var variableVar = "var";
    let variableLet = "let";
    const variableConst = "const Variable";
    // variableConst = "Ubah Constructor";

    let printOut = `
        variabel Var = ${variableVar}<br>
        variabel Let = ${variableLet}<br>
        variabel Const = ${variableConst}<br>
    `
    document.getElementById('variable').innerHTML = printOut
}