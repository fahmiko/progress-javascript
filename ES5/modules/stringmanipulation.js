export default function stringconversion() {

    let stringExample = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let sentence = "Lorem ipsum dolor sit amet, consectetur adipisicing elit"
    let split_sentence = sentence.split(" ");

    let printOut = `
        String = ${stringExample}<br>
        String Sentence = ${sentence}<br><br>
        String lenght = ` + stringExample.length + `<br>
        String locate(ipsum) = ` + sentence.indexOf('ipsum') + `<br>
        String search(ipsum) = ` + sentence.search('ipsum') + `<br>
        String search(search) = ` + sentence.search('search') + `<br>
        String Slice(6, 15)  = ` + sentence.slice(6, 15) + `<br>
        String Replace(ipsum to doror)  = ` + sentence.replace('ipsum', 'dolor') + `<br>
        String Concat  = ` + stringExample.concat(sentence) + `<br>
        String Trim  = ` + sentence.trim() + `<br>
        String split  = ` + split_sentence + `<br>
    `
    document.getElementById('string_c').innerHTML = printOut
}