export default () => {
    let cars = ["Toyota", "Nissan", "Honda"]
    let car_legth = cars.length
    let car_last = cars[cars.length - 1]
    // Array Method

    let car_join = cars.join(" , ")

    let car_looping = () => {
        var valueLoop = ""
        cars.forEach(car => {
            valueLoop += car + " "
        });
        return valueLoop
    }

    let printOut = `
        Array Legth   = ${car_legth} <br>
        Array Last   = ${car_last} <br>
        Array Join = ${car_join}<br>
        Array Looping = ` + car_looping() + ` 
    `

    document.getElementById('arrays').innerHTML = printOut

}