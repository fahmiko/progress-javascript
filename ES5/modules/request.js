const URL = 'https://jsonplaceholder.typicode.com/posts/'
export async function rget() {
    let posts = []
    try {
        const request = await fetch(URL)
        posts = await request.json()
    } catch (error) {

    }

    let table = `
        <table border='1'>
            <tr>
                <td>Post ID</td>
                <td>Name</td>
                <td>Content</td>
            </tr>
    `;

    for (var i = 0; i < 10; i++) {
        table += `<tr>`
        table += `<td>${posts[i].id}</td>`
        table += `<td>${posts[i].title}</td>`
        table += `<td>${posts[i].body}</td>`
        table += `</tr>`
    }

    table += `</table>`

    document.getElementById('requests').innerHTML = table
}

export async function rpost(post) {
    try {
        const request = await fetch(URL, {
            method: 'post',
            body: JSON.stringify(post),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        })

        console.log(await request.json())

    } catch (error) {

    }
}

export async function rlast() {
    try {
        let posts = []
        const request = await fetch(URL + "?id=101")

        posts = await request.json()

        console.log(posts)
    } catch (error) {
        console.log(error)
    }
}