export default function conditional() {
    let s1 = "1"
    let s2 = 1

    let printOut = `
        <p>a = ${s1} String</p>
        <p>b = ${s2} Int</p>
        <p>kondisi(manipulation) = ` + (s1 = s2) + `</p>
        <p>kondisi(equal) == ` + (s1 == s2) + `</p>
        <p>kondisi(equalto) === false </p>
        <p>kondisi(lessthen) <= ` + (s1 <= s2) + `</p>
        <p>kondisi(biggerthen) >= ` + (s1 >= s2) + `</p>
        <p>kondisi(bigger) > ` + (s1 > s2) + `</p>
        <p>kondisi(less) < ` + (s1 < s2) + `</p>
        <p>kondisi(notequal) ` + (s1 != s2) + ` </p>
    `

    document.getElementById('conditional').innerHTML = printOut


}