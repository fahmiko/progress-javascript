export default function datatypes() {
    let dt_string = "Hallo"
    let dt_int = 123
    let dt_int2 = 123
    let dt_boolean = true

    let dt_array = ['satu', 'dua', 'tiga']
    let dt_object = {
        name: 'Fahmiko',
        age: 20
    }
    let dt_array_object = [{
            number: 1
        },
        {
            number: 2
        }
    ]

    let printOut = `
        variabel string = ${dt_string} <br>
        variabel string + int = ` + (dt_string + dt_int) + `<br>
        variabel int + int = ` + (dt_int + dt_int2) + `<br>
        variabel boolean = ${dt_boolean} <br>
        variabel array = ${dt_array} <br>
        variabel object = ` + JSON.stringify(dt_object) + `
    `
    document.getElementById('datatype').innerHTML = printOut
}