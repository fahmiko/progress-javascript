export function promise() {
    let status = true
    let bproses = document.getElementById('btn_promise')
    let progress = new Promise(function (resolve, reject) {
        if (status) {
            resolve("Success")
        } else {
            reject("Error")
        }
    })

    bproses.addEventListener('click', () => {
        progress.then((response) => {
            console.log(response)
        }).catch((message) => {
            console.log(message)
        })
    })
}

export async function _async() {
    let posts = []
    try {
        const response = await fetch('https://jsonplaceholder.typicode.com/posts')
        posts = await response.json()
    } catch (error) {
        console.log(error)
    }
    // console.log(posts.splice(0, 5))
}