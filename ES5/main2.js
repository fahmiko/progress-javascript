// Modules
import login from './modules/login.js'
import register from './modules/register.js'
import users from './modules/datauser.js'

// Variable
var blogin = document.getElementById('submit')
var bregister = document.getElementById('register')
var bheader = document.getElementById('bheader')

// Function Register
blogin.addEventListener('submit', event => {
    event.preventDefault()
    login(users)
})

bheader.addEventListener('click', event => {
    event.preventDefault()
    if (bheader.value = 'Logout') {
        sessionStorage.clear()
        bheader.innerHTML = 'Login'
    }
})

if (JSON.parse(sessionStorage.getItem('users')) != null) {
    bheader.innerHTML = 'Logout'
} else {
    bheader.innerHTML = 'Login'
}