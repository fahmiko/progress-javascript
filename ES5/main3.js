// Modules
import login from './modules/login.js'
import register from './modules/register.js'
import users from './modules/datauser.js'
import {
    create,
    read,
    update,
    del
} from './modules/crud.js'

// Variable
var bheader = document.getElementById('bheader')
var bsubmit = document.getElementById('postForm')
var iname = document.getElementById('name')
var itype = document.getElementById('type')

// function
bheader.addEventListener('click', event => {
    event.preventDefault()
    if (bheader.value = 'Logout') {
        sessionStorage.clear()
        bheader.innerHTML = 'Login'
        location.reload()
    }
})

bsubmit.addEventListener('submit', event => {
    event.preventDefault()
    create({
        name: iname.value,
        type: itype.value
    })
})


if (JSON.parse(sessionStorage.getItem('users')) != null) {
    bheader.innerHTML = 'Logout'
} else {
    location.href = 'login.html'
    alert('belum login')
    bheader.innerHTML = 'Login'
}

read()