import Index from "@/view/Index";
import user from "@/router/user/";
import kategori from "@/router/kategori";
import subkategori from "@/router/subkategori";
import transaksi from "@/router/transaksi";
import tag from "@/router/tag";
import akun from "@/router/akun";

let router = [];
router.push(user);
router.push(kategori);
router.push(subkategori);
router.push(transaksi);
router.push(tag);
router.push(akun);

let routes = {
    path: "/crud",
    component: Index,
    meta: { requiresAuth: true },
    children: router
};

export default routes;
