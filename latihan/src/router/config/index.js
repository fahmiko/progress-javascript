import NProgress from "nprogress";
export default (store, router) => {
    router.beforeEach(((to, from, next) => {
        if (to.matched.some(record => record.meta.requiresAuth)) {
            if (sessionStorage.getItem('token') == null) {
                alert('Harap login terlebih dahulu')
                next({path: '/login'})
            } else {
                next()
            }
        } else {
            next()
        }
    }))

    router.beforeResolve(((to, from, next) => {
        if (to.name) {
            NProgress.start()
        }
        next()
    }))

    router.afterEach(() => {
        NProgress.done()
    })
}