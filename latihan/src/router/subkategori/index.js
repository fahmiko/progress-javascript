import Index from '@/view/subkategori/Index'
import List from '@/view/subkategori/List'

let routes = {
    path: 'subkategori',
    component: Index,
    meta: {requiresAuth: true},
    children: [{
        path: '/',
        component: List
    }]
}

export default routes