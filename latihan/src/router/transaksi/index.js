import Index from '@/view/transaksi/Index'
import List from '@/view/transaksi/List'
import Create from '@/view/transaksi/Create'

const routes = {
    path: 'transaksi',
    component: Index,
    meta: {requiresAuth: true},
    children: [{
        path: '/',
        component: List
    },{
        path: 'create',
        component: Create
    }]
}

export default routes