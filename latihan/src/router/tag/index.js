import Index from '@/view/tag/Index'
import List from '@/view/tag/List'
let routes = {
    path: 'tag',
    component: Index,
    meta: {requiresAuth: true},
    children: [
        {
            path: '/',
            component: List
        }
    ]
}


export default routes