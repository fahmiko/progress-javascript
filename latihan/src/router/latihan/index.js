import Index from '@/view/komponen/Index'
import Tugas1 from '@/view/komponen/Tugas1'
import Tugas2 from '@/view/komponen/Tugas2'
import Tugas3 from '@/view/komponen/Tugas3'
import Tugas4 from '@/view/komponen/Tugas4'
const routes = {
    path: '/latihan',
    component: Index,
    children: [{
            path: '/',
            component: Tugas1
        },
        {
            path: 'modal',
            component: Tugas2
        },
        {
            path: 'mixin',
            component: Tugas3
        }, {
            path: 'vuex',
            component: Tugas4
        }
    ]
}

export default routes