import Index from '@/view/akun/Index'
import List from '@/view/akun/List'
let routes = {
    path: 'akun',
    component: Index,
    meta: { requiresAuth: true },
    children: [
        {
            path: '/',
            component: List
        }
        ]
}

export default routes