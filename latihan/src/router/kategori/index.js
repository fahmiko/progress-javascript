import Index from '@/view/kategori/Index'
import List from '@/view/kategori/List'
import Edit from '@/view/kategori/Edit'
import Create from '@/view/kategori/Create'

let routes = {
    path: 'kategori',
    component: Index,
    meta: {requiresAuth: true},
    children: [{
        path: '/',
        component: List
    }, {
        path: 'edit/:id',
        component: Edit
    }, {
        path: 'create',
        component: Create
    }]
}

export default routes