import Login from '@/view/user/Login'

const routes = {
    path: '/login',
    component: Login
}

export default routes