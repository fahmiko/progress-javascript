// import user from '@/router/user'
// import latihan from '@/router/latihan'
// import Index from '@/view/Index'
import Vue from "vue";
import VueRouter from "vue-router";

import crud from "@/router/crud";
import latihan from "@/router/latihan";
import login from "@/router/login";
import Home from "@/view/Home";
import News from "@/view/News";
// import globaly from "@/router/globaly";

// console.log(globaly);

Vue.use(VueRouter);

let routes = [];
routes.push(crud);
routes.push(latihan);
routes.push(login);
routes.push({
    path: "/",
    meta: { requiresAuth: true },
    component: Home
});

routes.push({
    path: "/news",
    component: News
});

routes.push({
    path: "/user/profile",
    meta: { requiresAuth: true },
    component: () => import("@/view/user/Profile")
});

const router = new VueRouter({
    mode: "history",
    routes
});

export default router;
