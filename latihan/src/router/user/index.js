import Create from '@/view/user/Create'
import Index from '@/view/user/Index'
import List from '@/view/user/List'
import Edit from '@/view/user/Edit'

const routes = {
    path: 'user',
    component: Index,
    props: true,
    children: [
        {
            path: '/',
            component: List,
        },
        {
            path: 'edit/:id',
            component: Edit
        },
        {
            path: 'create',
            component: Create
        }
    ]
}

export default routes