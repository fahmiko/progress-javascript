import Vue from 'vue'
import Vuex from 'vuex'
import crud from '@/store/modules/crud'
import user from '@/store/modules/user'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
      isLoading: false
    },
    modules: {
        crud, user
    }
})

export default store