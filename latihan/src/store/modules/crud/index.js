import axios from "@/helpers/axios.js";
const namespaced = true;
export default {
    namespaced,
    state: {
        posts: [],
        progress: false
    },
    getters: {
        posts(state) {
            return Array.of(state.posts);
        },
        GET_HEADER(state) {
            return Object.keys(state.posts);
        }
    },
    mutations: {
        SET_POST(state, posts) {
            state.posts = posts;
        },
        SET_PROGRESS(state, status) {
            state.progress = status;
        }
    },
    actions: {
        async fetchData({ commit, rootState }, url) {
            rootState.isLoading = true;
            let request = await axios.get(url);
            let response = await request;
            commit("SET_POST", response.data.data);

            rootState.isLoading = false;
            return response.data.data;
        },
        async loadPost({ commit }, url) {
            return new Promise((resolve, reject) => {
                axios
                    .get(url)
                    .then(res => {
                        if (res.data.status == "success") {
                            commit("SET_POST", res.data.data);
                        } else {
                            reject(false);
                        }
                    })
                    .catch(error => {
                        reject(error);
                    });
            });
        },
        async postData({ rootState }, params) {
            rootState.isLoading = true;
            let request = await axios.post(params.url, params.data);
            if (params.headers != null) {
                request = await axios.post(
                    params.url,
                    params.data,
                    params.headers
                );
            }

            let response = await request.data;
            rootState.isLoading = false;
            return response;
        },
        async updateData({ commit, dispatch }, params) {
            axios
                .put(params.url, params.data)
                .then(commit("SET_PROGRESS", true))
                .then(res => {
                    if (res.data.status == "success") {
                        commit("SET_POST", res.data.data);
                        dispatch("loadPost", params.url);
                        this.$router.back();
                    }
                    commit("SET_PROGRESS", false);
                });
        },
        async deleteData({ dispatch, rootState }, params) {
            rootState.isLoading = true;
            let request = await axios.delete(params.url);
            let response = await request.data;
            if (response.status == "success") {
                dispatch("loadPost", params.url);
            }
            rootState.isLoading = false;
            return response;
        }
    }
};
