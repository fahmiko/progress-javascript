import axios from '@/helpers/axios.js'

const namespaced = true
const token = sessionStorage.getItem('token')
export default {
    namespaced,
    state: {
        token: token,
        dark: false
    },
    getters: {
        GET_THEME(state){
            return state.dark
        }
    },
    mutations: {
        SET_TOKEN(state, token) {
            sessionStorage.setItem('token', token)
            state.token = token
        },
        SET_THEME(state, theme){
            sessionStorage.setItem('theme', theme)
            state.dark = theme
        },
        REMOVE_TOKEN(state) {
            sessionStorage.removeItem('token')
            state.token = null
        }
    },
    actions: {
        async login({commit}, params) {
            let request = await axios.post(params.url, params.data);
            let response = await request.data;
            if(response.status == 'success'){
                commit('SET_TOKEN', response.data.token)
            }
            return response
        },
        logout({commit}, router) {
            router.push({path: '/login'})
            commit('REMOVE_TOKEN')
        }
    }
}
