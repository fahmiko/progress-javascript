// src/plugins/vuetify.js

import Vue from 'vue'
import Vuetify from 'vuetify'
import '@mdi/font/css/materialdesignicons.css'
Vue.use(Vuetify)

const opts = {
    icons: {
        iconfont: ['mdi'], // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4'
    },
    theme: {
        dark: true
    }
}

export default new Vuetify(opts)