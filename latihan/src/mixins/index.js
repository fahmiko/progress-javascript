import Vue from "vue";
import { durration, localDate, arrayToObj } from "@/utils/format";

let mixin = {
    methods: {
        breadcrump: function(url) {
            let arr_url = url.split("/");
            let breadcrumb = [];
            arr_url.forEach((value, index) => {
                if (index != 0) {
                    breadcrumb.push({ text: value, to: value });
                }
            });

            return breadcrumb;
        },
        $_randomColor: function() {
            return "#" + Math.floor(Math.random() * 16777215).toString(16);
        },
        $_pagination: function(data, paging, current) {
            let pagination = () => {
                if (this.current != 1) {
                    let start = paging * (current - 1) + 1;
                    return this.posts.slice(start, start + paging);
                } else {
                    return this.posts.slice(0, paging);
                }
            };

            return pagination();
        },
        $_global: function() {
            return "tes global";
        },
        $_durasi: function(start, end) {
            return durration(start, end);
        },
        $_localDate: function(date, weekday, year, month, day) {
            return localDate(date, weekday, year, month, day);
        },
        $_arrayToObj: function(array, key) {
            return arrayToObj(array, key);
        },
        $_downloadFile: function(url) {
            const link = document.createElement("a");
            link.href = url;
            window.open(link);
            return true;
        },
        $_inArray: function(array, value) {
            let found = -1;
            array.forEach(key => {
                if (key == value) found++;
            });
            return found;
        },
        $_fileType: function(fileName) {
            if (fileName == null) return false;
            let fileExtension = fileName.split(".");
            let index = fileExtension.length;
            return fileExtension[index - 1];
        }
    }
};

Vue.mixin(mixin);

export default mixin;
