export function durration(start, end) {
    let startDate = new Date(start);
    let endDate = new Date(end);
    console.log(endDate);
    console.log(startDate);

    return new Date();
}

export function localDate(date, weekday, year, month, day) {
    const event = new Date(date);
    const options = { weekday: weekday, year: year, month: month, day: day };
    return event.toLocaleDateString("id-ID", options);
}

export function arrayToObj(array, key) {
    const initialValue = {};
    return array.reduce((obj, item) => {
        return {
            ...obj,
            [item[key]]: item
        };
    }, initialValue);
}
