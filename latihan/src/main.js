import Vue from "vue";
import App from "./App.vue";
// import firebase from "firebase";
// import VueFire from 'vue-fire'
import "../node_modules/nprogress/nprogress.css";

import router from "./router";
import store from "@/store";
import config from "@/router/config";
import vuetify from "./plugins/vuetify";
// import firebaseConfig from './plugins/firebase'
import "vuetify/dist/vuetify.min.css";

import "nprogress";
import "@/mixins";
import "@/components";
import "@/helpers/axios.js";

// Vue.use(VueFire)
Vue.config.productionTip = false;
config(store, router);

new Vue({
     vuetify,
     router,
     store,
     render: h => h(App)
}).$mount("#app");
