import axios from 'axios'
import Vue from "vue";
import NProgress from 'nprogress';
import router from "../router";
import store from "../store";

NProgress.configure({ showSpinner: false })

let BASE_URL = 'http://127.0.0.1:8000/api/'

const instance = axios.create({
    baseURL: BASE_URL
})

instance.interceptors.request.use(config => {
    NProgress.start()
    config.headers.Authorization = 'Bearer ' + sessionStorage.getItem('token')
    return config
}, error => Promise.reject(error))


instance.interceptors.response.use(response => {
    NProgress.done()
    if(response.data.code == 300){
        alert('Token expired');
        store.dispatch('user/logout', router);
    }
    return response
})
Vue.prototype.$axios = instance;

export default instance